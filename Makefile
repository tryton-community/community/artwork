# -*- indent-tabs-mode: t -*-

.phony: clean all

FILES:=favicon.ico logo-256x256.png logo-64x64.png

all: $(FILES)

clean:
	rm -f $(FILES)

favicon.ico: logo.svg
	convert  $< -bordercolor white -border 0 \
		\( -clone 0 -resize 16x16 \) \
		\( -clone 0 -resize 32x32 \) \
		\( -clone 0 -resize 48x48 \) \
		\( -clone 0 -resize 64x64 \) \
		-delete 0 -alpha off -colors 256 \
		$@

logo-%.png: logo.svg
	convert $< -resize $(subst logo-,,$(subst .png,,$@)) $@
