Artwork for Tryton Community
============================

Based on the official Tryton artwork.

Our colors are:

- yellow: #f5db2b
- black: #1b2019

The Font used for the logo is “kimberley”,
which is free to be used within artwork,
but must not be redistributed.
You van download a free copy at
https://typodermicfonts.com/kimberley/.


Attribution & License
---------------------

The *Tryton Community* logo and banner are
made by Hartmut Goebel,
based on the Tryton logo and banner by Cédric Krier,
and licensed under |CC-BY-SA|_ (CC-BY-SA).


Building the images
---------------------

Building the pixel images from the SVG requires

- make
- imagemagick


.. |CC-BY-SA| replace:: Creative Commons Attribution-Share Alike 4.0 International
.. _CC-BY-SA: http://creativecommons.org/licenses/by-sa/4.0/
